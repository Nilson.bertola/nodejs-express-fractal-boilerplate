const app = (module.exports = require('express')());

app.use('/healthcheck', require('./healthcheck'));
